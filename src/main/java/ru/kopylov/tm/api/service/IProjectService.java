package ru.kopylov.tm.api.service;

import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void save(final Project project);

    List<Project> listAll();

    Project findById(final String id);

    void deleteById (final String id);

    List<Project> findAll();

}
