package ru.kopylov.tm.api.service;

import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void save(final Task project);

    List<Task> listAll();

    Task findById(final String id);

    void deleteById (final String id);

}
