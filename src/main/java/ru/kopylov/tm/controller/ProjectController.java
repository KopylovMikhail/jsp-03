package ru.kopylov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.service.ProjectService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public ModelAndView projectList() {
        final List<Project> projects = projectService.listAll();
        final ModelAndView modelAndView = new ModelAndView("projects");
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @GetMapping("/project_view/{id}")
    public ModelAndView viewProject(@PathVariable final String id) {
        final Project project = projectService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("project", project);
        modelAndView.setViewName("project_view");
        return modelAndView;
    }

    @GetMapping("/project_edit/{id}")
    public ModelAndView editProject(@PathVariable final String id) {
        final Project project = projectService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("project", project);
        modelAndView.setViewName("project_edit");
        return modelAndView;
    }

    @PostMapping("/project_edit/edit")
    public String saveProject(
            @ModelAttribute("id") final String id,
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateStart") final String dateStart,
            @ModelAttribute("dateFinish") final String dateFinish,
            @ModelAttribute("state") final String state
    ) throws ParseException {
        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateStart));
        project.setDateFinish(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateFinish));
        project.setState(State.valueOf(state));
        projectService.save(project);
        return "redirect:/project_view/" + project.getId();
    }

    @GetMapping("/project_remove/{id}")
    public String removeProject(@PathVariable String id) {
        projectService.deleteById(id);
        return "redirect:/projects";
    }

    @GetMapping("/project_create")
    public ModelAndView createProject() {
        String dateStart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
        String dateFinish = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project_create");
        modelAndView.addObject("dateStart", dateStart);
        modelAndView.addObject("dateFinish", dateFinish);
        return modelAndView;
    }

    @PostMapping("/add_project")
    public String addProject(
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateStart") final String dateStart,
            @ModelAttribute("dateFinish") final String dateFinish,
            @ModelAttribute("state") final String state
    ) throws ParseException {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setDateStart(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateStart));
        project.setDateFinish(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateFinish));
        project.setState(State.valueOf(state));
        projectService.save(project);
        return "redirect:/project_view/" + project.getId();
    }

}
