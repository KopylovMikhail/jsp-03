package ru.kopylov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;
import ru.kopylov.tm.service.ProjectService;
import ru.kopylov.tm.service.TaskService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    ProjectService projectService;

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public ModelAndView taskList() {
        final List<Task> tasks = taskService.listAll();
        final ModelAndView modelAndView = new ModelAndView("tasks");
        modelAndView.addObject("tasks", tasks);
        return modelAndView;
    }

    @GetMapping("/task_view/{id}")
    public ModelAndView viewTask(@PathVariable final String id) {
        final Task task = taskService.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", task);
        modelAndView.setViewName("task_view");
        return modelAndView;
    }

    @GetMapping("/task_edit/{id}")
    public ModelAndView editTask(@PathVariable final String id) {
        final Task task = taskService.findById(id);
        final List<Project> projects = projectService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projects);
        modelAndView.setViewName("task_edit");
        return modelAndView;
    }

    @PostMapping("/task_edit/edit")
    public String saveTask(
            @ModelAttribute("id") final String id,
            @ModelAttribute("projectId") final String projectId,
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateStart") final String dateStart,
            @ModelAttribute("dateFinish") final String dateFinish,
            @ModelAttribute("state") final String state
    ) throws ParseException {
        final Task task = new Task();
        final Project project = new Project();
        project.setId(projectId);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateStart));
        task.setDateFinish(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateFinish));
        task.setState(State.valueOf(state));
        task.setProject(project);
        taskService.save(task);
        return "redirect:/task_view/" + task.getId();
    }

    @GetMapping("/task_remove/{id}")
    public String removeTask(@PathVariable String id) {
        taskService.deleteById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/task_create")
    public ModelAndView createTask() {
        String dateStart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
        String dateFinish = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task_create");
        modelAndView.addObject("dateStart", dateStart);
        modelAndView.addObject("dateFinish", dateFinish);
        return modelAndView;
    }

    @PostMapping("/add_task")
    public String addProject(
            @ModelAttribute("name") final String name,
            @ModelAttribute("description") final String description,
            @ModelAttribute("dateStart") final String dateStart,
            @ModelAttribute("dateFinish") final String dateFinish,
            @ModelAttribute("state") final String state
    ) throws ParseException {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setDateStart(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateStart));
        task.setDateFinish(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(dateFinish));
        task.setState(State.valueOf(state));
        taskService.save(task);
        return "redirect:/task_view/" + task.getId();
    }

}
