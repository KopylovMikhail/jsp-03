package ru.kopylov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.repository.ProjectRepository;

import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    public void save(final Project project) {
        projectRepository.save(project);
    }

    public List<Project> listAll() {
        return projectRepository.findAll();
    }

    public Project findById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    public void deleteById (final String id) {
        projectRepository.deleteById(id);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

}
