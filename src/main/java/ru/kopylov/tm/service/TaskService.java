package ru.kopylov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    public void save(final Task project) {
        taskRepository.save(project);
    }

    public List<Task> listAll() {
        return taskRepository.findAll();
    }

    public Task findById(final String id) {
        return taskRepository.findById(id).get();
    }

    public void deleteById (final String id) {
        taskRepository.deleteById(id);
    }

}
