<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>PROJECTS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/projects">PROJECTS</a><jsp:text> | </jsp:text><a href="/tasks">TASKS</a></h2>
<div align="center">
    <h2>TASK EDIT</h2>
    <form:form action="edit" method="post">
        <table border="1" cellpadding="5">
            <tr>
                <th colspan="2">TASK</th>
            </tr>
            <tr>
                <th>ID</th>
                <th>
                    ${task.id}
                    <input type="hidden" name="id" placeholder="${task.id}" readonly="readonly" value="${task.id}"/>
                </th>
            </tr>
            <tr>
                <th>Project</th>
                <th>
                    <input type="text" list="project-list" name="projectId" placeholder="Choose project" required/>
                    <datalist id="project-list">
                        <c:forEach items="${projects}" var="project">
                        <option label="${project.name}" value="${project.id}">
                            </c:forEach>
                    </datalist>
                </th>
            </tr>
            <tr>
                <th>Name</th>
                <th>
                    <input type="text" name="name" value="${task.name}"/>
                </th>
            </tr>
            <tr>
                <th>Description</th>
                <th>
                    <input type="text" name="description" value="${task.description}"/>
                </th>
            </tr>
            <tr>
                <th>Date Start</th>
                <th>
                    <input type="datetime" name="dateStart" value="${task.dateStart}"
                           pattern="[0-9]{4}-\d\d-\d\d\s\d\d:\d\d:\d\d.[0-9]{,3}" placeholder="yyyy-MM-dd HH:mm:ss.S"/>
                </th>
            </tr>
            <tr>
                <th>Date Finish</th>
                <th>
                    <input type="datetime" name="dateFinish" value="${task.dateFinish}"
                           pattern="[0-9]{4}-\d\d-\d\d\s\d\d:\d\d:\d\d.[0-9]{,3}" placeholder="yyyy-MM-dd HH:mm:ss.S"/>
                </th>
            </tr>
            <tr>
                <th>State</th>
                <th>
<%--                    <input type="text" list="state-list" name="state" value="${project.state}"/>--%>
                    <input type="text" list="state-list" name="state" placeholder="Choose state" required/>
                    <datalist id="state-list">
                        <option value="PLANNED">
                        <option value="PROCESS">
                        <option value="DONE">
                    </datalist>
                </th>
            </tr>
            <tr>
                <th colspan="2"><button type="submit">Save</button></th>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
