<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>TASKS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/projects">PROJECTS</a><jsp:text> | </jsp:text><a href="/tasks">TASKS</a></h2>
    <div align="center">
        <h2>TASK MANAGEMENT</h2>
        <table border="1" cellpadding="5">
            <tr>
                <th colspan="7">TASKS</th>
            </tr>
            <tr>
                <th>Nr</th>
                <th>ID</th>
                <th>Name</th>
                <th>Description</th>
                <th>View</th>
                <th>Edit</th>
                <th>Remove</th>
            </tr>
            <c:forEach items="${tasks}" var="task">
                <tr>
                    <td></td>
                    <td>${task.id}</td>
                    <td>${task.name}</td>
                    <td>${task.description}</td>
                    <td>
                        <a href="/task_view/${task.id}">View</a>
                    </td>
                    <td>
                        <a href="/task_edit/${task.id}">Edit</a>
                    </td>
                    <td>
                        <a href="/task_remove/${task.id}">Remove</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <h4><a href="/task_create">CREATE TASK</a></h4>
    </div>
</body>
</html>
